using System;
using System.Collections.Generic;
using System.Linq;

namespace SegundoParcial
{
     public class Productos
        {
            public int ID { get; set; }
            public string Nombre { get; set; }
            public int Precio { get; set; }
            public int Existencia { get; set; }
        }
        
    class Program
    {
        static void Main(string[] args)
        {
            int[] Dinero = new int[] { 5, 10, 25, 50, 100, 200 };

            List<Productos> productos = new List<Productos>();
            int[] ID = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string[] Nombres = { "Refresco","Agua","Hershey","M&M","Oreo","Doritos","Muffin","Mamuts","Cheetos","Malta Morena"};
            int[] Precios = { 25, 10, 50, 50, 30, 25, 20, 120, 30, 25 };
            int[] Existencia = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };

            for (int i = 0; i < Nombres.Length; i++)
            {
                productos.Add(new Productos() { ID = ID[i], Nombre = Nombres[i], Precio = Precios[i], Existencia = Existencia[i] });
            }

            Console.WriteLine("--MAQUINA EXPENDEDORA--");
            
            foreach (Productos Articulos in productos)
            {
                Console.WriteLine($"{Articulos.ID} {Articulos.Nombre} -- Precio: {Articulos.Precio}");
            }
            Console.WriteLine();
            Console.Write("Introduce el dinero: ");

            try {
                int DineroIngresado = int.Parse(Console.ReadLine());
                int DineroTotal = 0;

                if (Dinero.Contains(DineroIngresado))
                {
                    DineroTotal = DineroIngresado;
                    Console.Write("Eligir El Articulo: ");
                    int opcion = int.Parse(Console.ReadLine());
                    var data = productos.Where(x => x.ID == opcion).ToList();
                    foreach (var i in data)
                    {
                        Console.WriteLine($"{i.Nombre} -- {i.Precio}$ ");
                        Console.WriteLine("");
                        Console.WriteLine("-------------");
                        Console.WriteLine("1 - Confirmar");
                        Console.WriteLine("2 - Cancelar");
                        Console.WriteLine("-------------");
                        Console.Write("Elige una opcion: ");
                        int confirmar = int.Parse(Console.ReadLine());
                        if (confirmar == 1)
                        {
                            Console.WriteLine("");
                            int compra = ( DineroTotal - i.Precio);
                            if (compra >= 0)
                            {
                                int Devuelta = (DineroTotal - i.Precio);

                                Console.WriteLine($"Compra confirmada, dinero restante: {Devuelta}");
                                Console.WriteLine($"Articulo elegido: {i.Nombre}");
                                i.Existencia-= 1;
                                Console.WriteLine($"Existencia: {i.Existencia}");
                            }
                            else
                            {
                                Console.WriteLine("La cantidad de dinero es insuficiente");
                            }
                        }
                        else
                        {
                            Console.WriteLine($"Cancelando compra");
                        }
                    }

                }
                else
                {
                    Console.WriteLine("Error");
                }
            }
           
            catch (Exception) 
            {
               Console.WriteLine("Error");
            }
            Console.ReadKey();
        }
       
    }
}
